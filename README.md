## Promises

The `Promise` object represents the eventual completion (or failure) of an **asynchronous** operation and its resulting value.
A `Promise` is a _proxy_ for a value not necessarily known when the promise is created. It allows you to associate handlers with an asynchronous action's eventual success value or failure reason. This lets asynchronous methods return values like synchronous methods: instead of immediately returning the final value, the asynchronous method returns a promise to supply the value at some point in the future.
A Promise is in one of these states:

The promise object returned by the new Promise constructor has these internal properties:

- state — initially "pending", then changes to either "fulfilled" when resolve is called or "rejected" when reject is called.
- result — initially undefined, then changes to value when resolve(value) called or error when reject(error) is called.

A pending promise can either be fulfilled with a value or rejected with a reason (error). When either of these options happens, the associated handlers queued up by a promise's then method are called. If the promise has already been fulfilled or rejected when a corresponding handler is attached, the handler will be called, so there is no race condition between an asynchronous operation completing and its handlers being attached.

```javascript
var promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("foo");
  }, 300);
});
```

### Consumers: Callbacks and Async/Await

If we resolve a Promise, then callbacks are executed. Else, it means that the Promise is rejected and the catch callbacks are executed.

```javascript
// using a callback
promise
  .then((value) => {
    console.log("resolved callback", value);
  })
  .catch((error) => {
    console.log("rejected", error);
  });
```

```javascript
// using Async Await
(async () => {
    const one = await promise;
    console.log('resolved async', one);
  })();
```
